// ==UserScript==
// @name         Avatar Hidden
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       zero_up
// @match        https://vkplay.live/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';
    GM_addStyle('[class*="ChatMessage_avatar_"] { display: none; }');
})();
